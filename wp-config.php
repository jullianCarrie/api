<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en « wp-config.php » et remplir les
 * valeurs.
 *
 * Ce fichier contient les réglages de configuration suivants :
 *
 * Réglages MySQL
 * Préfixe de table
 * Clés secrètes
 * Langue utilisée
 * ABSPATH
 *
 * @link https://fr.wordpress.org/support/article/editing-wp-config-php/.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define( 'DB_NAME', 'api_wp' );

/** Utilisateur de la base de données MySQL. */
define( 'DB_USER', 'root' );

/** Mot de passe de la base de données MySQL. */
define( 'DB_PASSWORD', '' );

/** Adresse de l’hébergement MySQL. */
define( 'DB_HOST', 'localhost' );

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/**
 * Type de collation de la base de données.
 * N’y touchez que si vous savez ce que vous faites.
 */
define( 'DB_COLLATE', '' );

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clés secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Bcnk~#&;.3FR?AcyZnIs#2p9sgXRy`%f)Rd?+-Nr%QAbVhw_U/#s576zze.n.SWg' );
define( 'SECURE_AUTH_KEY',  '`ST}v5?DZ.&qm&{aE35JpUNCJgxx5*&:e`]BFlLva[?8D!tC|IH&5[n@#Y65#p-]' );
define( 'LOGGED_IN_KEY',    '(5vCIc{+9<&84lG{p70E<A%`hK@Dvz=lka?,ZCZ=XCvCOJ) zQ|H5]%V8JB~9_mX' );
define( 'NONCE_KEY',        'sb&PKDKfd/*(olnhsM3toX<W RrOh`]X3N|s&Zc^!}$,(dhvXwyI[5#lTdA[XSOb' );
define( 'AUTH_SALT',        'm,@R&3Hv&1xP.@L]L[k.>[C3(P1QC&##_pGR^c;v*F1)*KLU>_KZyFW8A9e;F6rv' );
define( 'SECURE_AUTH_SALT', '1~I3},ORUBTL`<9A3%n]tV<v<m0L~yZziL46IuH;FZ~N&m0dy!!:cu_d4kg8{:s_' );
define( 'LOGGED_IN_SALT',   'Lt_<d))4O<SQnJOEpX<Kcf:BWvNrr= SJ|@Xk/OXWAzBlyY_<]gc-^!;M-&x$:Dt' );
define( 'NONCE_SALT',       'Mn#:9mvVi+]&Yi+m(5h-uesuqd9f.!F6kl[5]raHYo}a|^3.[Q[k2Et|n)B2*+c4' );
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortement recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://fr.wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* C’est tout, ne touchez pas à ce qui suit ! Bonne publication. */

/** Chemin absolu vers le dossier de WordPress. */
if ( ! defined( 'ABSPATH' ) )
  define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once( ABSPATH . 'wp-settings.php' );
