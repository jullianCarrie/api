
<?php
/* 
Plugin Name: Plugin api
Description: Mon premier plugin !
Author: Moi
*/
 




function api_name_post_type() {
 


  // On peut définir ici d'autres options pour notre custom post type

  $args = array(
    'label'               => ( 'Apprenants'),
    'description'         => ( 'liste aprenants'),
    'menu_icon'           => 'dashicons-admin-users',
    'public'              => true,
    "publicly_queryable"  => true,
	"show_ui"             => true,
	"show_in_rest"        => true,
	"rest_base" => "",
	"rest_controller_class" => "WP_REST_Posts_Controller",
	"has_archive"         => false,
	"show_in_menu"        => true,
	"show_in_nav_menus"   => true,
	"delete_with_user"    => false,
	"exclude_from_search" => false,
	"capability_type" => "post",
	"map_meta_cap"        => true,
	"hierarchical"        => false,
	"rewrite" => [ "slug" => "nom_prenom", "with_front" => true ],
	"query_var"           => true,
	"supports"            => [ "title", "thumbnail", "custom-fields" ],
	"show_in_graphql" => true,

  );

  // On enregistre notre custom post type qu'on nomme ici "serietv" et ses arguments
  register_post_type( 'aprenants', $args );
 
}
 
add_action( 'init', 'api_name_post_type' );
 
// Hook the 'wp_footer' action hook, add the function named 'mfp_Add_Text' to it
add_action("wp_footer", "mfp_Add_Text");
 
// Define 'mfp_Add_Text'
function mfp_Add_Text()
{
  echo "<p style='color: black;'>After the footer is loaded, my text is added!</p>";
}


function api_promotion() {

	/**
	 * Taxonomy: Promotions 2021.
	 */

	$labels = [
		"name" => __( "Promotion", "appreants" ),
		"singular_name" => __( "Promotion", "apprenants" ),
	];

	
	$args = [
		"label"                    => __( "Promotions", "Apprenants" ),
		"labels"                   => $labels,
		"public"                   => true,
		"publicly_queryable"       => true,
		"hierarchical"             => false,
		"show_ui"                  => true,
		"show_in_menu"             => true,
		"show_in_nav_menus"        => true,
		"query_var"                => true,
		"rewrite"                  => [ 'slug' => 'promotion', 'with_front' => true, ],
		"show_admin_column"        => false,
		"show_in_rest"             => true,
		"show_tagcloud"            => false,
		"rest_base"                => "promotion",
		"rest_controller_class"    => "WP_REST_Terms_Controller",
		"show_in_quick_edit"       => false,
		"show_in_graphql"          => false,
	];
	register_taxonomy( "promotion", [ "aprenants" ], $args );
}
add_action( 'init', 'api_promotion' );

function api_competence() {

	/**
	 * Taxonomy: Promotions 2022.
	 */

	$labels = [
		"name" => __( "Compétence", "apprenants" ),
		"singular_name" => __( "competence", "apprenant" ),
	];

	
	$args = [
		"label" => __( "competence", "competences" ),
		"labels" => $labels,
		"public" => true,
		"publicly_queryable" => true,
		"hierarchical" => false,
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => [ 'slug' => 'competence', 'with_front' => true, ],
		"show_admin_column" => false,
		"show_in_rest" => true,
		"show_tagcloud" => false,
		"rest_base" => "competence",
		"rest_controller_class" => "WP_REST_Terms_Controller",
		"show_in_quick_edit" => false,
		"show_in_graphql" => false,
	];
	register_taxonomy( "competence", [ "aprenants" ], $args );
}
add_action( 'init', 'api_competence' );

 
/*information wp ajout metaboxe*/
add_action('add_meta_boxes', 'init_metabox_appc26');
/*function appelée lors initialisation de l'admin */
function init_metabox_appc26()
{
    /*ajout de la metaboxe (id metaboxe,titre metaboxe,function metaboxe,slug contenu affiché*/
    add_meta_box('metabox_api', 'Informations', 'metabox_api', 'apprenants');
}
/*construction de la function metaboxe*/
function metabox_api($post)
{
    /*récup valeur actuelle pour mettre dans le champ*/
    $prenom = get_post_meta($post->ID, '_mon_prenom', true);
    $lien_linkendin = get_post_meta($post->ID, '_mon_lien_linkendin', true);
    $portfolio = get_post_meta($post->ID, '_mon_portfolio', true);
    $lien_cv = get_post_meta($post->ID, '_mon_lien_cv', true);
    /*affichage metaboxe*/
    echo '<label for="prenom">Prénom : </label>';
    echo '<input id="prenom" type="text" name="prenom" value="' . $prenom . '" /> ';
    echo '<label for="lien_linkendin">Lien Linkendin : </label>';
    echo '<input id="lien_linkendin" type="url" name="lien_linkendin" value="' . $lien_linkendin . '" />';
    echo '<label for="portfolio">Lien Portfolio : </label>';
    echo '<input id="portfolio" type="url" name="portfolio" value="' . $portfolio . '" />';
    echo '<label for="lien_cv">CV  : </label>';
    echo '<input id="lien_cv" type="url" name="lien_cv" value="' . $lien_cv . '" />';
}

/*sauvegarde données metabox */
add_action('save_post', 'save_metaboxes');
function save_metaboxes($post_id)
{
    /** si metaboxe définie(isset) alors sauvegarde de sa valeur */
    if (isset($_POST['prenom'])) {
        update_post_meta($post_id, '_mon_prenom', esc_html($_POST['prenom']));
    }
    if (isset($_POST['lien_linkendin'])) {
        update_post_meta($post_id, '_mon_lien_linkendin', esc_html($_POST['lien_linkendin']));
    }
    if (isset($_POST['portfolio'])) {
        update_post_meta($post_id, '_mon_portfolio', esc_html($_POST['portfolio']));
    }
    if (isset($_POST['lien_cv'])) {
        update_post_meta($post_id, '_mon_lien_cv', esc_html($_POST['lien_cv']));
    }
}

function filter_apprenantslist_json($data, $post, $context)
{
    $prenom = get_post_meta($post->ID, '_mon_prenom', true);
    $lien_linkendin = get_post_meta($post->ID, '_mon_lien_linkendin', true);
    $portfolio = get_post_meta($post->ID, '_mon_portfolio', true);
    $lien_cv = get_post_meta($post->ID, '_mon_lien_cv', true);

    if ($prenom) {
        $data->data['prenom'] = $prenom;
    }
    if ($lien_linkendin) {
        $data->data['lien_linkendin'] = $lien_linkendin;
    }

    if ($portfolio) {
        $data->data['portfolio'] = $portfolio;
    }

    if ($lien_cv) {
        $data->data['lien_cv'] = $lien_cv;
    }

    return $data;
}
add_filter('rest_prepare_apprenants', 'filter_apprenantslist_json', 10, 3);

function acf_to_rest_api($response, $post, $request) {
    if (!function_exists('get_fields')) return $response;

    if (isset($post)) {
        $acf = get_fields($post->id);
        $response->data['acf'] = $acf;
    }
    return $response;
}
add_filter('rest_prepare_apprenants', 'acf_to_rest_api', 10, 3);
?>

